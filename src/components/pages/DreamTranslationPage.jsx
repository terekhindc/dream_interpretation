import DreamInput from '../components/InputForm.jsx';
import DreamInterpretation from '../components/DreamInterpretation.jsx';
import '../../styles/DreamTranslationPage.css';

const DreamTranslationPage = () => {
    return (
        <div className="page-container">
            <header className="header">
                <div className="logo">Terekhin Digital Crew</div>
            </header>
            <main className="main-content">
                <div className="dream-translation">
                    <h1>MugenEki (夢幻訳) Dream Translation</h1>
                    <DreamInput />
                    <DreamInterpretation />
                </div>
                <div className="artwork">
                    <img src="/path/to/artwork.png" alt="Dream Artwork" />
                </div>
            </main>
            <footer className="footer">
                <p>Website created by Terekhin DC, 2024</p>
            </footer>
        </div>
    );
};

export default DreamTranslationPage;