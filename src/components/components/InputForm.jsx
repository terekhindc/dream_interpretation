const DreamInput = () => {
    return (
        <div className="dream-input">
            <textarea placeholder="Describe your dream here..." />
        </div>
    );
};

export default DreamInput;