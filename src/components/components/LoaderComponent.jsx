import styled, { keyframes } from 'styled-components';

const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

const spinReverse = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(-360deg); }
`;

const pulse = keyframes`
  0% { r: 10; opacity: 1; }
  50% { r: 20; opacity: 0.5; }
  100% { r: 10; opacity: 1; }
`;

const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  background: none;
`;

const StyledSVG = styled.svg`
  margin: auto;
  display: block;
`;

const Circle1 = styled.circle`
  stroke: #888;
  stroke-width: 6;
  fill: none;
  stroke-dasharray: 47.12388980384689 47.12388980384689;
  animation: ${spin} 2s linear infinite;
`;

const Circle2 = styled.circle`
  stroke: #ccc;
  stroke-width: 4;
  fill: none;
  stroke-dasharray: 31.41592653589793 31.41592653589793;
  animation: ${spinReverse} 1.5s linear infinite;
`;

const Circle3 = styled.circle`
  fill: #888;
  animation: ${pulse} 2s infinite;
`;

const Loader = () => (
    <LoaderWrapper>
        <StyledSVG xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="100px" height="100px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
            <g transform="translate(50,50)">
                <Circle1 cx="0" cy="0" r="30" />
                <Circle2 cx="0" cy="0" r="20" />
                <Circle3 cx="0" cy="0" r="10" />
            </g>
        </StyledSVG>
    </LoaderWrapper>
);

export default Loader;