import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Helmet, HelmetProvider } from 'react-helmet-async';

const AdSense = ({ client, slot, format = 'auto', responsive = 'false' }) => {
    useEffect(() => {
        const loadAds = () => {
            try {
                (window.adsbygoogle = window.adsbygoogle || []).push({});
            } catch (e) {
                console.error('AdSense error', e);
            }
        };
        loadAds();
    }, []);

    return (
        <HelmetProvider>
            <Helmet>
                <script
                    async
                    src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
                ></script>
            </Helmet>
            <ins
                className="adsbygoogle"
                style={{ display: 'block' }}
                data-ad-client={client}
                data-ad-slot={slot}
                data-ad-format={format}
                data-full-width-responsive={responsive}
            ></ins>
        </HelmetProvider>
    );
};

AdSense.propTypes = {
    client: PropTypes.string.isRequired,
    slot: PropTypes.string.isRequired,
    format: PropTypes.string,
    responsive: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

export default AdSense;