import PropTypes from 'prop-types';

const AdSenseAuto = ({ client, slot }) => {
    return (
        <>
            <ins
                className="adsbygoogle"
                style={{ display: 'block', backgroundColor: 'black', padding: '10px' }}
                data-ad-client={client}
                data-ad-slot={slot}
                data-ad-format="auto"
                data-full-width-responsive="true"
                data-adtest="on"
            ></ins>
        </>
    );
};

AdSenseAuto.propTypes = {
    client: PropTypes.string.isRequired,
    slot: PropTypes.string.isRequired,
};

export default AdSenseAuto;
