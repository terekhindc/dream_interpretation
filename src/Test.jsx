import React from 'react';
import ThreeColumnLayout from './layouts/three_column_layout.jsx';

function App() {
    return (
        <ThreeColumnLayout/>
    )
}

export default App;