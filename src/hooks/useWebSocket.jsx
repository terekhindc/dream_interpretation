import { createContext, useContext, useState, useEffect } from 'react';
import PropTypes from "prop-types";

const WebSocketContext = createContext(null);

export const useWebSocket = () => useContext(WebSocketContext);

export const WebSocketProvider = ({ children, uri }) => {
    const [clientId, setClientId] = useState(null);
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        const socket = new WebSocket(uri);

        socket.onopen = () => {
            console.log('Connected to WebSocket server');
        };

        socket.onmessage = (event) => {
            const data = JSON.parse(event.data);
            if (data.type === 'registered') {
                console.log('Registered with ID:', data.id);
                setClientId(data.id);
            } else if (data.type === 'response') {
                console.log('Received response:', data);
                setMessages(prev => [...prev, data]);
            }
        };

        socket.onerror = (error) => {
            console.error('WebSocket error:', error);
        };

        socket.onclose = () => {
            console.log('WebSocket connection closed');
        };

        return () => {
            socket.close();
        };
    }, [uri]);

    return (
        <WebSocketContext.Provider value={{ clientId, messages }}>
            {children}
        </WebSocketContext.Provider>
    );
};

WebSocketProvider.propTypes = {
    children: PropTypes.node.isRequired,
    uri: PropTypes.string.isRequired,
};