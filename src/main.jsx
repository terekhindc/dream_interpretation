import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './styles/global.css'
import './styles/fonts.css'
import './styles/logo.css'
import './styles/mobile.css'
import {WebSocketProvider} from "./hooks/useWebSocket.jsx";

ReactDOM.createRoot(document.getElementById('root')).render(
    <WebSocketProvider uri='wss://api.mugeneki.com'>
        <App />
    </WebSocketProvider>
)
