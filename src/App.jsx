import './App.css';
import {useEffect, useState} from 'react';
import DOMPurify from 'dompurify';
import mainImage from './assets/DreamGirlImage.svg';
import Loader from './components/components/LoaderComponent.jsx';
import Advert from './components/components/advert/AdsTerra.jsx';
import {getInterpretation} from "./services/apiService.js";
import {useWebSocket} from "./hooks/useWebSocket.jsx";

function App() {
    const [description, setDescription] = useState('');
    const [interpretation, setInterpretation] = useState('');
    const [error, setError] = useState('');
    const { clientId, messages } = useWebSocket();
    const [state, setState] = useState('waiting');

    const maxLengthDescription = 5000;

    useEffect(() => {
        if (messages.length > 0) {
            const lastMessage = messages[messages.length - 1];
            if (lastMessage.type === 'response') {
                setInterpretation(lastMessage.data);
                setState('finished');
            }
        }
    }, [messages]);

    const handleChange = (e) => {
        if (e.currentTarget.value.length <= maxLengthDescription)
            setDescription(e.currentTarget.value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!description) {
            setError('Please enter a dream description.');
            return;
        }
        setError('');
        setInterpretation('');

        if (!clientId) {
            console.error('Client ID is not available');
            return;
        }

        setState('processing');
        try {
            await getInterpretation(clientId, description);
        } catch (error) {
            console.error('Error fetching project data:', error);
            setState('waiting');
        }
    };

    const convertToParagraphs = (text) => {
        let paragraphs = text.split('\n').map(str => `<p>${str}</p>`).join('');
        paragraphs += '<p>If you want to know more about AI and AR/VR software development, visit our website: <a href="https://www.terekhindt.com">www.terekhindt.com</a>.</p>';
        return paragraphs;
    };

    const sanitizedInterpretation = DOMPurify.sanitize(convertToParagraphs(interpretation));
    return (
        <div className="container">
            <div className="sidebar">
                <div className="vertical-text">Website created by Terekhin DC, 2024</div>
            </div>
            <div className="content">
                <div className="content-left">
                    <div className="form-container">
                        <h1>MugenEki (夢幻訳)</h1>
                        <h1>Dream Translation</h1>
                        <form onSubmit={handleSubmit}>
                            <textarea
                                value={description}
                                onChange={handleChange}
                                placeholder="I'm looking forward to hearing about your dream! Describe it here in any language you prefer."
                                rows="6"
                                cols="30"
                            />
                            <p className='text-small'>{maxLengthDescription - description.length} characters remaining</p>
                            <button type="submit" disabled={state==='processing'}>Interpret Dream</button>
                            <br /><br />
                            <div>
                                <p className='text-small'>At Dream Interpreter, we use advanced AI powered to provide detailed dream interpretations based on renowned psychological theories. Trust our insights drawn from the works of Carl Jung, Sigmund Freud, and other leading psychologists. Understand your subconscious mind with our reliable and accurate interpretations.</p>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="content-center">
                    <div className="result-container">
                        {state==='processing' && (
                            <div>
                                <div className="spinner"></div>
                                <p>Please wait! We are processing your request…</p>
                                <Loader />
                            </div>
                        )}
                        {state==='finished' && (
                            <div className="interpretation" dangerouslySetInnerHTML={{ __html: sanitizedInterpretation }}/>
                        )}
                        {error && (
                            <div className="error">{error}
                                <Advert />
                            </div>
                        )}
                    </div>
                </div>
                <div className="content-right">
                    <a href="https://www.terekhindt.com">
                        <div className="logo-container">
                            <div className="circle">
                                <div className="circle-text">Terekhin Digital Crew</div>
                            </div>
                        </div>
                    </a>
                    <img src={mainImage} alt="Dreams"/>
                </div>
            </div>
            <div className="sidebar"></div>
        </div>
    );
}

export default App;