import React from 'react';
import './three_column_layout.module.css';
import Copyrights from '../components/copyrights.jsx';

const ThreeColumnLayout = ({ children }) => {
    return (
        <div className="main-container">
            <div className="left-container">
                <Copyrights/>
            </div>
            <div className="content-container">
                {children}
            </div>
            <div className="right-container">
                {/* Right Container Content */}
            </div>
        </div>
    );
};

export default ThreeColumnLayout;