import axios from 'axios';

//const api_uri = 'http://localhost/api/interpret-dream';
const api_uri = 'https://api.mugeneki.com/api/interpret-dream';

export const getInterpretation = async (clientId, description) => {
    if (!clientId) {
        throw new Error('Client ID is required');
    }

    console.log({ clientId, description })
    try {
        const response = await axios.post(api_uri, { clientId, description }, {
            headers: {
                'Content-Type': 'application/json'
            },
            withCredentials: true
        });
        return response.data;
    } catch (error) {
        console.error('Error getting project', error);
        throw error;
    }
};